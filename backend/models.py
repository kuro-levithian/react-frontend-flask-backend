import datetime
from email.policy import default

from flask_sqlalchemy import SQLAlchemy
from uuid import uuid4

db = SQLAlchemy()

def get_uuid():
    return uuid4().hex

class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.String(32), primary_key=True, unique=True, default = get_uuid())
    email = db.Column(db.String(345),unique = True)
    password = db.Column(db.Text, nullable = False)

class Post(db.Model):
    __tablename__ = "post"
    id = db.Column(db.Integer, primary_key=True, unique=True)
    title = db.Column(db.String(100))
    detail = db.Column(db.Text())
    date = db.Column(db.DateTime, default=datetime.datetime.now())

    def __init__(self, title, detail):
        self.title = title
        self.detail = detail