from turtle import title
from flask import Flask, request, jsonify, session
from flask_bcrypt import Bcrypt
from flask_cors import CORS,cross_origin
from flask_session import Session
from flask_marshmallow import Marshmallow
from config import ApplicationConfig
from models import db, User, Post
from uuid import uuid4

app = Flask(__name__)
app.config.from_object(ApplicationConfig)

bcrypt: Bcrypt = Bcrypt(app)
CORS(app, supports_credentials=True)
server_session = Session(app)
db.init_app(app)
ma = Marshmallow(app)

with app.app_context():
    db.create_all()

class PostSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'detail', 'date')

post_schema = PostSchema()
posts_schema = PostSchema(many=True)

class UserSchema(ma.Schema):
    class Meta:
        fields = ('id','email','password')
user_schema = UserSchema()
users_schema = UserSchema(many=True)

# User section for login and  register new user
@app.route("/@me")
def get_current_user():
    user_id = session.get("user_id")

    if not user_id:
        return (jsonify({"error": "Can not find user with this email"}), 401)

    user = User.query.filter(User.id == user_id).first()
    return jsonify({
        "id": user.id,
        "email": user.email
    })

@app.route("/all")
def load_all_user():
    userlist = User.query.all()
    result = users_schema.dump(userlist)
    return jsonify(result)

@app.route("/register", methods=["POST"])
def register_user():
    id = uuid4().hex
    email = request.json["email"]
    password = request.json["password"]

    user_exists = User.query.filter(User.email == email).first() is not None

    if user_exists:
        return (jsonify({"error": "User already exist"}), 409)

    hashed_password = bcrypt.generate_password_hash(password)
    new_user = User(id=id, email=email, password=hashed_password)
    db.session.add(new_user)
    db.session.commit()

    return jsonify({
        "id": new_user.id,
        "email": new_user.email
    })

@app.route("/login", methods=["POST"])
def login_user():
    email = request.json["email"]
    password = request.json["password"]

    user = User.query.filter(User.email == email).first()

    if user is None:
        return (jsonify({"error": "Can not find user with this email"}), 401)

    if not bcrypt.check_password_hash(user.password, password):
        return (jsonify({"error": "Can not find user with this password"}), 401)

    session["user_id"] = user.id

    return jsonify({
        "id": user.id,
        "email": user.email
    })

# Post sections for create new post and get presented post

##Get all posts
@app.route("/get_all_posts", methods = ["GET"])
def get_all_posts():
    all_post = Post.query.all()
    results = posts_schema.dump(all_post)
    return jsonify(results)

##Get a specific post
@app.route("/get_post/<id>/", methods = ["GET"])
def get_post(id):

    post = Post.query.get(id)
    return post_schema.jsonify(post)

##Create new post
@app.route("/new_post",methods = ["POST"])
def create_post():
    title = request.json["title"]
    detail = request.json["detail"]

    new_post = Post(title=title, detail=detail)
    db.session.add(new_post)
    db.session.commit()

    return post_schema.jsonify(new_post)

##Update a specific post
@app.route("/update_post/<id>/", methods = ["PUT"])
def update_post(id):
    post = Post.query.get(id)

    new_title = request.json["title"]
    new_detail = request.json["detail"]

    if(new_title is None):
        post.detail = new_detail
        db.session.commit()
    if(new_detail is None):
        post.title = new_title
        db.session.commit()

    post.title = new_title
    post.detail = new_detail
    db.session.commit()

    return post_schema.jsonify(post)

##Delete a specific post
@app.route("/delete_post/<id>/", methods = ["DELETE"])
def delete_post(id):
    post = Post.query.get(id)
    db.session.delete(post)
    db.session.commit()

    return post_schema.jsonify(post)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
