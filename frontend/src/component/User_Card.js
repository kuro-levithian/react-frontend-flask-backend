import React from 'react'

const User_Card = ({user}) => {
  return (
    <div className="card">
        <h3>{ user.email }</h3>
    </div>
  )
}

export default User_Card