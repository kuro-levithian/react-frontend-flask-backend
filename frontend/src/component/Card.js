import React from 'react'

const Card = ({post}) => {
  return (
    <div className="card">
        <h3>{ post.title }</h3>
        <p>{post.detail}</p>
        <p>Create time : {post.date}</p>
        <div>
            <button className='update-button'>Update Post</button>
            <button className='delete-button'>Delete Post</button>
        </div>
    </div>
  )
}

export default Card