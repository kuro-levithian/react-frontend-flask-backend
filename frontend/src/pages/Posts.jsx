import React,{useState} from 'react'
import { QueryClient, QueryClientProvider, useQuery, useMutation } from 'react-query'
import Card from '../component/Card';
import axios from 'axios';

const fetchPosts = async() => {
    const res = await fetch ("http://127.0.0.1:5000/get_all_posts");
    return res.json();
}

const addPost = async(new_post) =>{
  const res = axios.post("http://127.0.0.1:5000/new_post",new_post)
  return res
}

const queryClient = new QueryClient()

const Posts = () => {

    const [title, setTitle] = useState("")
    const [detail, setDetail] = useState("")

    const { isLoading, isError, data, error } = useQuery('posts', fetchPosts)


    if (isLoading) {
      return <span>Loading...</span>
    }

    if(isError){
      return <span>Error: {error.message}</span>
    }
    
    const handleOnClick = (e) =>{
      const new_post = {title, detail}
      addPost(new_post)
    }

    return (
      <div>
        <h2>Post</h2>
          {data.map(post => (
            <Card key={post.id} post={post}/>
          ))}
        <div className='center'>
          <h3 style={{color:"black"}}>New Post</h3>
          <form>
            <div className="inputbox">
              <input type="text" required="required" onChange={(e)=>setTitle(e.target.value)}/>
              <span>Title</span>
            </div>
            <div className="inputbox">
              <textarea type="text" required="required" onChange={(e)=>setDetail(e.target.value)}/>
              <span>Detail</span>
            </div>
            <div className="inputbox">
              <button type='submit' className='new-post-button' onClick={handleOnClick}>Submit</button>
            </div>
          </form>
        </div>
        <a href='/'>User List</a>
     </div>
    )
}

const hof = (WrappedComponent) => {
  // Its job is to return a react component wrapping the baby component
  return (props) => (
      <QueryClientProvider client={queryClient}>
          <WrappedComponent {...props} />
      </QueryClientProvider>
  );
};

export default hof(Posts)