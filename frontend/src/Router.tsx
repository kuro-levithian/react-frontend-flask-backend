import React from 'react'
import { BrowserRouter, Routes ,Route } from 'react-router-dom'
import UserPage from './pages/UsersPage'
import NotFound from './pages/404'
import Loginpage from './pages/Loginpage'
import Registerpage from './pages/Registerpage'
import Posts from './pages/Posts'

const Router = () => {
  return (
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<UserPage/>}/>
            <Route path='/login' element={<Loginpage/>}/>
            <Route path='/register' element={<Registerpage/>}/>
            <Route path='/posts' element={<Posts/>}/>
        </Routes>
    </BrowserRouter>
  )
}

export default Router